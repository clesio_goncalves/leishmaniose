import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID";  # The GPU id to use, usually either "0" or "1";
os.environ["CUDA_VISIBLE_DEVICES"]="2";  # Do other imports now...

from keras.applications.resnet50 import ResNet50
from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from keras.applications.inception_v3 import InceptionV3
from keras.applications import NASNetLarge

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

import pandas as pd
import numpy as np
from glob import glob


path = '/data/romuere/formas/rodar/*/'
batch_size = 32
databases = glob(path)
databases = databases[0:1]

models = [ResNet50(weights='imagenet',include_top=True), VGG16(weights='imagenet',include_top=True), VGG19(weights='imagenet',include_top=True),
          InceptionV3(weights='imagenet',include_top=True), NASNetLarge(weights='imagenet',include_top=True)]

out = '/home/romuere/features/'
if not os.path.exists(out):
    os.mkdir(out)

for base in databases:
    print('---------------------------------------------------------------------')
    print('Running database {}'.format(base))
    data = ImageDataGenerator(rescale=1./255)
    for model in models:
        data_gen = data.flow_from_directory(base,target_size=(model.input.shape[1],
                                                      model.input.shape[2]),
                                    batch_size=batch_size)
        print('Running model {}'.format(model.name))
        model.layers.pop()
        model = Model(inputs=model.input,outputs=model.layers[-1].output)
        features = model.predict(data_gen)
        save = np.concatenate([features, np.reshape(data_gen.labels,(-1,1))],axis=1)
        np.savetxt('{}/{}_{}.csv'.format(out,base.split('/')[-2],model.name),save,delimiter=',')
