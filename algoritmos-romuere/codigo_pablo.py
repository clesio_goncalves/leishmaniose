# Ele usa o ImageDataGenerator para ler as imagens pra treinar a CNN

#ImageDataGenerator(rescale=1./255, 
#	fill_mode="nearest" # adiciona bordas de zeros.
	
# ImageDataGenerator pra treino e validação

# a) Pegando a CNN pré treinada no ImageNet:
base_model = VGG19(weights='imagenet', include_top=False, input_shape = (image_size, image_size, 3))

# B) Adicionando as camadas totalmente conectadas da MLP: Adicionando uma nova mlp:
x = Flatten(name='flatten')(base_model.output)
x = Dense(1024, activation='relu', name='fc1')(x)
x = Dropout(0.5)(x)
x = Dense(2, activation='softmax', name='predictions')(x)

# C) Juntando A CNN pré treinada com a MLP:
model = Model(inputs=base_model.input, outputs=x)


# D) Descongelando todas as camadas, para ajuste fino profundo:
for layer in model.layers[:]:
    layer.trainable = True # Treinavel
    #layer.trainable = False # Não treinavel
    
# E) Verificando se o modelo ficou correto:
model.summary()

# Para extração de características tu tem que fazer o seguinte:
# F) Carregar o modelo salvo:
path_model = 'model.h5'
base_model = load_model(path_model)
base_model.summary()


# G) remover a camada de predição, se for usar as características da penúltima camada totalmente conectada:
# É o padrão, mais eu não uso assim. Testa primeiro assim e depois te digo como eu gosto de fazer pra ve se tem resultado melhor
base_model = keras.models.Model(inputs=model.get_input_at(0), outputs=base_model.layers[-2].get_output_at(0))
base_model.summary()

# Extraindo as carácteristicas:
# H) Predizendo a imagem, para pegar as carácteristicas:
# Carregando a imagem e já fazendo o resize:
path_img = 'img.png'
img = image.load_img(path_img, target_size=(224, 224))
# Transformando a imagem em um array:
x = image.img_to_array(img)
# Expandindo:
x = np.expand_dims(x, axis=0)
# Processando:
x = preprocess_input(x)
# Extraindo as features:
features = model.predict(x)
